# UBCG: Phylogenetic analysis of bacteria
## Tutorial for SCELSE employees
## Dependencies (latest)
* fasttree
* hmmer
* mafft
* ncbi-genome-download
* prodigal
* raxml

## Install virtual environemnt/package manager
### Miniconda (good way to manage packages needed for UBCG) for Linux
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

### Press ENTER to review the license agreement and type `yes` to agree to the license terms

### Do **NOT** confirm Miniconda3 to be installed in your user directory (`/home/user/miniconda3`)

### Instead, specify a different location by providing the path to your scratch directory:
```
/gpfs1/user/scratch/miniconda3
```

### When prompted if the installer should initialize Miniconda3 by running conda init - type `yes`

### After the installation, source the .bashrc file located in your home directory
```
source ~/.bashrc
```

### Create UBCG environment (must be in the respective order to show priority) 
```
conda config --add channels bioconda
conda config --add channels conda-forge
conda create --name ubcg fasttree hmmer mafft ncbi-genome-download prodigal raxml
```

### If you wish not to activate the base environment upon login in, execute the following code:
```
conda config --set auto_activate_base false
```

### Note: It is worthwhile to use [TaxSeedo](https://bitbucket.org/ryanjameskennedy/taxseedoinfo/src/master/) to identify the query genome. The genomes of TaxSeedo's top hits can be downloaded using ncbi-genome-download.

## Set up UBCG
### Download UBCG
```
wget https://www.ezbiocloud.net/download2/download_ubcg -O UBCG.zip
unzip UBCG.zip
```

### The UBCG analysis must be performed within the unzipped UBCG directory
```
cd UBCG
```

## Shell scripting
### Add the following code but change the `user`:
```
source /gpfs1/scratch/user/miniconda3/etc/profile.d/conda.sh
export PATH=/gpfs1/scratch/user/miniconda3/envs/ubcg/bin:$PATH
export LD_LIBRARY_PATH=/gpfs1/scratch/user/miniconda3/envs/ubcg/lib:$LD_LIBRARY_PATH
conda activate ubcg
```

### Retrieve closely related species (specify the `<genus>`)
```
ncbi-genome-download -s genbank -F fasta -g <genus> -o data/NCBI -p 64 bacteria
gunzip data/NCBI/genbank/bacteria/*/*.gz
```

### Gene extraction
```
for INPUT in data/NCBI/genbank/bacteria/*/*.fna
do
    LABEL=$(head -n 1 $INPUT | cut -d"," -f1 )
    java -jar UBCG.jar extract -bcg_dir bcg -i $INPUT -label $LABEL -t 64
done
```

### Extract the query genes (specify the `<query_label>`)
```
java -jar UBCG.jar extract -bcg_dir bcg -i $INPUT -label <query_label> -t 64
```

### Alignment (specify the `<project_name>`)
```
java -jar UBCG.jar align -bcg_dir bcg -prefix <project_name>
```

## Visualisation
### The output file (Newick format) can be visualised using [Figtree](https://github.com/rambaut/figtree/releases) (the `project_name` will be the same as specified in the previous step)
```
output/project_name/project_name.UBCG_gsi(92).codon.50.label.nwk
```

### Note: When attempting to classify bacterial species, it may be of interest to use [DDDH](http://ggdc.dsmz.de/ggdc.php) as well as ANI ([FastANI](https://github.com/ParBLiSS/FastANI), [OrthoANI](https://github.com/althonos/orthoani), etc.) to assist in the identification process.

## Contact
### Ryan James Kennedy (Research Associate)
#### Nanyang Technological University, Singapore Centre for Environmental Life Sciences Engineering
#### E: <ryan.kennedy@ntu.edu.sg> | W: [SCELSE website](www.scelse.sg)